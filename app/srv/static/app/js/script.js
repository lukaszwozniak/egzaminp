$( document ).ready(function() {
    var progressbar = '<div class="progress">\
                        <div class="progress-bar bar active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">\
                         </div>\
                        </div>';
    var close_button = '<div class="modal-footer">\
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>\
                       </div>';
    var PENDING_COUNTER = 0
    //start code from django.doc
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    })
    //end of code from django.doc
    $(".download_btn").click(function( event ) {
        event.preventDefault();

        var data = $.parseJSON($("#courses_json").attr("value"));
        if(!isNaN($(this).attr("value"))){
            data = data[$(this).attr("value")]
            $('#progressModalLabel').text("Status: Zadanie przekazane do przetwarzania");
            $('.modal-body').html(progressbar);
            $('.modal-footer').remove();
            $('#progressModal').modal('show');
        }else
        {
            $($(this).attr("taret-label")).text("Status: Zadanie przekazane do przetwarzania");
            $('#modal-body'+$(this).attr("value")).show();
            $('.modal-footer').remove();
            $('#progressModal').modal('show');

        }
        var areq = $.ajax({
            type     : "POST",
            url      : $(this).attr("href"),
            data     : JSON.stringify(data),
            error:    function(error) {
                
                console.log(error);
                setError(error);
            }
        });


        $.when(areq).done(function(){
            resp = areq.responseJSON;
            if(resp.hasOwnProperty("result")){
                $('#progressModalLabel').text("Status: Paczka zip gotowa");
                setMessage(resp['result']);
            }else{
                getProgress(resp['job_url']);
            }
        });

    });
    function getProgress(job_url){
        $.ajax({
            type     : "GET",
            url      : job_url,
            data     : "",
            error: function(err) {
                setError(err);
            },
            complete: function(resp){
                reponse = resp.responseJSON;
                var status = reponse['job_status'];
                var result = reponse['job_result'];
                $('#progressModalLabel').text("Status: "+status);
                if(result.hasOwnProperty('current')){
                    setBar(result['current'], result['total']);
                    if(result['current']==0){
                        PENDING_COUNTER+=1
                        if(PENDING_COUNTER > 15){
                            var error = {status:"Worker nie działa na serwerze", statusText:"Aktualnie nie da się stworzyć paczki, proszę spróbować później"};
                            setError(error);
                            PENDING_COUNTER = 0;
                            return;
                        }
                    }
                }
                if(!result.hasOwnProperty('result')){
                setTimeout(function(){getProgress(job_url);}, 1000);
                }else{
                    zip_url = result['result'];
                    setMessage(zip_url);
                }
            }
        });
    }
    function setMessage(message){
        modal_body = 'Za chwile rozpocznie się pobieranie pliku. Jeśli plik nie pobiera się automatycznie, kliknij <a href="'+message+'">tutaj</a>';
        $('.modal-body').html(modal_body);
        $('.modal-content').append(close_button);
        window.setTimeout(function(){ window.location = message; }, 3000);
    }
    function setError(error){
        $('.modal-body').html(error.statusText);
        $('#progressModalLabel').text("Status: "+error.status);
        $('.modal-content').append(close_button);
    }
    function setBar(current, total){
         $('.bar').css('width', current+'%')
         $('.bar').text(current/total *100 + "%");
    }
});
