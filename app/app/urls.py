from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from views import Register, Login, Logout, Activate
from django.contrib import admin
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    url(r'^register/', Register, name='register'),
    url(r'^activate/(?P<activate_uuid>[-\w]+)/$', Activate, name='activate'),
    url(r'^login/', Login, name='login'),
    url(r'^logout/', Logout, name='logout'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^canvas/', include('django_canvas.urls')),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)
