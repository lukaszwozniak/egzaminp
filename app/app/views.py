# -*- coding=utf-8 -*-
from django.views.generic import View
from forms import *
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.views.decorators.http import require_http_methods
from models import *
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.core.mail import send_mail


@require_http_methods(["GET", "POST"])
def Login(request):
    purpose = 'Logowanie'
    if request.user.is_authenticated():
        messages.add_message(request, messages.INFO, 'Jesteś już zalogowany')
        return redirect('home')
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid() and form.get_user().is_active:
            login(request, form.get_user())
            messages.add_message(request, messages.SUCCESS, 'Zalogowano')
            return redirect('home')
        else:
            messages.add_message(request, messages.ERROR, 'Logowanie nie powiodło się')
    else:
        form = AuthenticationForm()
    return render(request, 'form.html', locals())


@require_http_methods(["GET"])
def Logout(request):
    if request.user.is_authenticated():
        logout(request)
        messages.add_message(request, messages.INFO, 'Zostałeś wylogowany pomyślnie')
    return redirect('home')


@require_http_methods(["GET", "POST"])
def Register(request):
    purpose = "Rejestracja"
    if request.method == 'POST':
        form = UserCreateForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            user_activate = UserActivate.create(request, user)
            user_activate.full_clean()
            user_activate.save()
            res = send_mail('%s %s' % (unicode("Potwierdzenie rejestracji użytkownika", "utf-8"), user),
                            'Aktywacja konta pod linkiem: %s' % user_activate,
                            'Rejstracja@canvas.herokuapp', [user.email], fail_silently=False)
            if res == 1:
                messages.add_message(request, messages.SUCCESS,
                                     'Rejestracja powiodła się, aby aktywować konto proszę sprawdzić skrzynkę e-mail')
            else:
                user.delete()
                messages.add_message(request, messages.WARNING,
                                     'Rejestracja nie powiodła się, problem z serwerem mailowym. Spróbuj później')
            return redirect('home')
        else:
            messages.add_message(request, messages.ERROR, 'Rejestracja nie powiodła się')
    else:
        form = UserCreateForm()
    return render(request, 'form.html', locals())


@require_http_methods(["GET"])
def Activate(request, activate_uuid):
    user_activate = UserActivate.objects.get(activate_uuid=activate_uuid)
    user = user_activate.user
    user.is_active = True
    user.save()
    messages.add_message(request, messages.SUCCESS, 'Twoje konto jest już aktywne. Możesz się teraz zalogować')
    user_activate.delete()
    return redirect('login')
