from django.db import models
from django.contrib.auth.models import User
from uuid import uuid4
from django.core.urlresolvers import reverse

class UserActivate(models.Model):
    activate_uuid = models.TextField()
    activate_url = models.URLField()
    user = models.ForeignKey(User, unique=True)

    @classmethod
    def create(cls, request, user):
        gen_id = str(uuid4())
        user_activate = cls(activate_uuid=gen_id,
                            activate_url=request.build_absolute_uri(reverse('activate',
                                                                            kwargs={'activate_uuid': gen_id})),
                            user=user)
        return user_activate

    def __unicode__(self):
        return self.activate_url