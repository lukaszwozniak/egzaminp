import os
from os import environ
import dj_database_url

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = environ.get(
    'SECRET_KEY',
    '##%he5sc1%^9oqlf=dq8ae&k1kw(17q^)-=%f7u$rq9)_d06g_')

# allow debug to be set from env config
#DEBUG = True if environ.get('DEBUG') is True else False
DEBUG = True
TEMPLATE_DEBUG = True
SESSION_EXPIRE_AT_BROWSER_CLOSE = False
SESSION_SAVE_EVERY_REQUEST = True
ALLOWED_HOSTS = ['.herokuapp.com']

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app',
    'gunicorn',
    'django_forms_bootstrap',
    'django_canvas',
    'south',
    'djcelery',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_tools.middlewares.ThreadLocal.ThreadLocalMiddleware',
)

ROOT_URLCONF = 'app.urls'

WSGI_APPLICATION = 'app.wsgi.application'

DATABASES = {}
DATABASES['default'] = dj_database_url.config()

LANGUAGE_CODE = 'pl'

TIME_ZONE = 'Europe/Warsaw'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

STATIC_URL = '/static/'

STATIC_ROOT = 'srv/static'

MEDIA_ROOT = os.path.join(BASE_DIR, '../../srv/media')

MEDIA_URL = '/media/'

LOGIN_URL = '/login/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, '../../srv/assets'),
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, '../templates/'),
)
LOGGING = {
    'version': 1,
}

from django.contrib.messages import constants as messages

MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(BASE_DIR, '../../srv/django.cache'),
    }
}

import djcelery
djcelery.setup_loader()
BROKER_URL = environ.get('CLOUDAMQP_URL', 'amqp://mjyytvqs:AsQ4quFsijo87DXisRJahyW_4zm2YAKg@bunny.cloudamqp.com/mjyytvqs')

EMAIL_HOST_USER = environ.get('SENDGRID_USERNAME', 'app26408989@heroku.com')
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_PASSWORD = environ.get('SENDGRID_PASSWORD', 'uwdvdxuy')

SENDGRID_EMAIL_HOST = "smtp.sendgrid.net"
SENDGRID_EMAIL_PORT = 587
SENDGRID_EMAIL_USERNAME = EMAIL_HOST_USER
SENDGRID_EMAIL_PASSWORD = EMAIL_HOST_PASSWORD