# -*- coding=utf-8 -*-
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from celery.result import AsyncResult
from canvasclient import CanvasClient
from forms import *
import json
from django.core.cache import get_cache
import traceback
import sys
from tasks import create_zip
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods

cache = get_cache('default')

CANVAS_LOGIN = '_canvas_user'

@require_http_methods(["GET", "POST"])
def CanvasLogin(request):
    canvas = "Canvas"
    purpose = "Logowanie"
    if request.session.get(CANVAS_LOGIN):
        messages.add_message(request, messages.SUCCESS, 'Jesteś zalogowany do Canvas [Na czas trwania sesji: %s sec]'
                                                        % request.session.get_expiry_age())
        return redirect('home')
    if request.method == 'POST':
        form = CanvasForm(data=request.POST)
        if form.is_valid(request):
            canvas_user = form.save(request)
            messages.add_message(request, messages.SUCCESS, 'Dodano konto Canvas [Na czas trwania sesji: %s sec]' % request.session.get_expiry_age())
            return redirect('home')
        else:
            messages.add_message(request, messages.ERROR, 'Nie udało się dodać konta Canvas')
    else:
        canvas_user = request.session.get(CANVAS_LOGIN)
        if canvas_user:
            form = CanvasForm(initial={'canvas_login': canvas_user['canvas_login'],
                                       'canvas_password': canvas_user['canvas_password']})
        else:
            form = CanvasForm()
    return render(request, 'form.html', locals())

@require_http_methods(["GET"])
def CanvasList(request):
    canvas_user = getCanvasUser(request)
    if canvas_user:
        canvas_client = CanvasClient(canvas_user['canvas_login'], canvas_user['canvas_password'])
        courses = canvas_client.get_courses()
        courses_json = json.dumps(courses)
        return render(request, 'django_canvas/list.html', locals())
    else:
        messages.add_message(request, messages.ERROR, 'Podaj login oraz hasło do Canvas')
        return redirect('canvas_login')

@require_http_methods(["GET"])
def GetSessionAge(request):
    return HttpResponse(json.dumps({'age': request.session.get_expiry_age()}),
                        content_type="application/json")

@require_http_methods(["GET"])
def ProlongSession(request):
    request.session.set_expiry(120)
    return HttpResponse(json.dumps({'prolong': "SUCCESS"}),
                        content_type="application/json")


@require_http_methods(["POST"])
def CanavasDownloadZip(request):
    if request.is_ajax():
        try:
            courses = json.loads(request.body)
            if type(courses) is dict:
                courses = [courses]
            #if courses in cache return cached link
            cache_key = sum(bytearray(str(courses)))
            cached_result = cache.get(cache_key)
            if cached_result:
                return HttpResponse(json.dumps({'result': cached_result}),
                                    content_type="application/json")
            else:
                result = create_zip.delay(getCanvasUser(request), courses, request.user)
                job_url = request.build_absolute_uri(reverse('canvas_job_progress', kwargs={'job_id': result.id}))
                return HttpResponse(json.dumps({'job_url': job_url, 'job_status': result.status}),
                                    content_type="application/json")
        except Exception as ex:
            print ex
            return HttpResponse(json.dumps(ex), status=500)



def JobProgress(request, job_id):
    try:
        task = AsyncResult(job_id)
        if task.result:
            if 'result' in task.result:
                task.result['result'] = request.build_absolute_uri(task.result.get('result'))
                #cache.set(task.result['cache_key'], task.result['result'], 300) #cache for 5 min
                cache_key = sum(bytearray(str(task.result['cache_key'])))
                cache.set(cache_key, task.result['result'], 300)
                data = {"job_status": u"Przetwarzanie zakończone powodzeniem", "job_result": task.result}
            else:
                data = {"job_status": task.state, "job_result": task.result}
        else:
                data = {"job_status": u"Zadanie w kolejce", "job_result": {'current': 0, 'total': 100}}
        return HttpResponse(json.dumps(data), content_type="application/json")
    except Exception as ex:
        return HttpResponse(json.dumps(str(ex)), status=500)


def getCanvasUser(request):
    try:
        return request.session.get(CANVAS_LOGIN)
    except IndexError:
        return None
