from django.views.generic import TemplateView
from django.conf.urls import patterns, include, url
from django.contrib import admin
from views import *

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^prefs/', CanvasLogin, name='canvas_login'),
                       url(r'^list/', CanvasList, name='canvas_list'),
                       url(r'^grades/', CanavasDownloadZip, name='canvas_grades'),
                       url(r'^progress/(?P<job_id>[-\w]+)/$', JobProgress, name='canvas_job_progress'),
                       url(r'^age/', GetSessionAge, name='canvas_age'),
                       url(r'^prolong', ProlongSession, name='canvas_prolong')
                       )
