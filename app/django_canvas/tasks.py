# -*- coding=utf-8 -*-
import os
from celery.utils.imports import *
import canvasclient
from celery import shared_task, current_task
from django.conf import settings
import zipfile
import time


@shared_task
def create_zip(canvas_user, courses, user):
    current_task.update_state(state=u'Ustanowienie połączenia z Canvas', meta={'current': 20, 'total': 100})
    #Tworzenie połączenia z Canvas
    canvas_client = canvasclient.CanvasClient(canvas_user['canvas_login'], canvas_user['canvas_password'])
    current_task.update_state(state=u'Połączono z Canvas', meta={'current': 30, 'total': 100})
    file_list = []
    current_task.update_state(state=u'Pobieranie ocen z Canvas', meta={'current': 30, 'total': 100})
    step = 40/len(courses)
    for course in courses:
        #pobieranie ocen dla poszczególnych zadań w kursie
        file_info = canvas_client.prepare_file(course, user)
        file_list.append(file_info['path'])
        current_task.update_state(state=u'Pobieranie ocen z Canvas', meta={'current': 30+step, 'total': 100})
    time.sleep(1)
    current_task.update_state(state=u'Tworzenie paczki zip', meta={'current': 70, 'total': 100})
    #tworzneie pliku zip
    if len(courses) > 1:
        zip_name = '%s/Oceny_%s.zip' % (str(user), canvas_user['canvas_login'])
    else:
        zip_name = '%s/Oceny_%s_%s.zip' % (str(user), canvas_user['canvas_login'], course['name'])
    zip_path = os.path.join(settings.MEDIA_ROOT, zip_name)
    zip_url = os.path.join(settings.MEDIA_URL, zip_name)
    with zipfile.ZipFile(zip_path, 'w') as zip_file:
        time.sleep(1)
        current_task.update_state(state=u'Tworzenie paczki zip', meta={'current': 90, 'total': 100})
        for file in file_list:
            zip_file.write(file, os.path.basename(file))
    time.sleep(1)
    current_task.update_state(state=u'Paczka zip gotowa do pobrania, za chwilę pojawi się link',
                              meta={'current': 100, 'total': 100})
    time.sleep(3)
    #plik zip gotowy, adres do pliku
    return {"result": zip_url, "cache_key": courses}