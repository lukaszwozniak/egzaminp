# -*- coding=utf-8 -*-
from django import forms
from requests.exceptions import HTTPError
from canvasclient import CanvasClient

CANVAS_LOGIN = '_canvas_user'

class CanvasForm(forms.Form):
    canvas_login = forms.CharField(required=True)
    canvas_password = forms.CharField(required=True, widget=forms.PasswordInput(render_value=True))

    def is_valid(self, request):
        valid = super(CanvasForm, self).is_valid()
        if not valid:
            return valid
        try:
            from django.forms.util import ErrorList
            self._errors['canvas_login'] = ErrorList()
            CanvasClient(self.cleaned_data['canvas_login'], self.cleaned_data['canvas_password'])
        except HTTPError:
            self._errors['canvas_login'].append('Login albo hasło do Canvas jest błędne.')
            return False
        return True

    def save(self, request):
        canvas_user = {'canvas_login': self.cleaned_data['canvas_login'],
                       'canvas_password': self.cleaned_data['canvas_password']}
        request.session[CANVAS_LOGIN] = canvas_user
        request.session.set_expiry(120)
        return canvas_user


