# -*- coding=utf-8 -*-
import json
import requests
import os
from django.conf import settings
import codecs

class CanvasClient():

    _BASE_URL = "https://canvas.instructure.com"
    _API_URL = '/api/v1'
    _use_raw_response = False
    _SESSION = None

    def __init__(self, user, password, use_raw_response=False):
        self._use_raw_response = use_raw_response
        self._SESSION = self.login(user, password)

    def request(self, method, url, *args, **kwargs):

        if self._SESSION:
            if len(args) > 1:
                args[1] = json.dumps(args[1])
            elif kwargs.get("data"):
                kwargs["data"] = json.dumps(kwargs["data"])
            url = "%s%s%s" % (self._BASE_URL, self._API_URL, url)
            request = requests.Request(method, url, *args, **kwargs)
            prepped = self._SESSION.prepare_request(request)
            response = self._SESSION.send(prepped)
            
            index = response.content.find(";") + 1
            if self._use_raw_response:
                return response
            return json.loads(response.content[index:])

    def login(self, user, password):
        session = requests.Session()
        url = "%s%s" % (self._BASE_URL, "/login?nonldap=true")
        payload = {'pseudonym_session[unique_id]': user, 'pseudonym_session[password]': password}
        resp = session.post(url, data=payload)
        resp.raise_for_status()
        return session

    def get_courses(self):
        return self.request("GET", "/courses?include[]=total_scores")

    def get_grades(self, course, sort=True):
        assignments = self.request("GET", '/courses/%s/assignments?include[]=submission' % course['id'])
        if sort:
            assignments = sorted(assignments, key=lambda k: (k['assignment_group_id'], k['due_at']))
        return assignments

    def prepare_file(self, course, user):
        file_name = '%s/%s.csv' % (str(user), course['name'])
        file_path = os.path.join(settings.MEDIA_ROOT, file_name)
        file_url = os.path.join(settings.MEDIA_URL, file_name)
        if not os.path.exists(os.path.dirname(file_path)):
            os.makedirs(os.path.dirname(file_path))
        with codecs.open(file_path, 'w', 'utf-8-sig') as file:
                file.write(u'%s\t%s\t%s\t%s\n' % ('Zadanie', 'Wynik', 'Termin dostarczenia', 'Id kategorii'))
                for assignment in self.get_grades(course):
                    line = u'%s\t%s/%s\t%s\t%s\n' % (assignment.get('name'),
                                                     float(CanvasClient.excludeNone(
                                                     assignment.get('submission').get('grade', 0.0))),
                                                     assignment.get('points_possible'),
                                                     assignment.get('due_at'),
                                                     assignment.get('assignment_group_id'))
                    file.write(line)
        return {"path": file_path, "url": file_url}

    @staticmethod
    def excludeNone(value):
        return 0.0 if value is None else value





