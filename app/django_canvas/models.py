from django.db import models
from django.contrib.auth.models import User


class CanvasUser(models.Model):
    canvas_login = models.CharField(max_length=100)
    canvas_password = models.CharField(max_length=100)
    user = models.ForeignKey(User)

    def toSession(self):
        return {"canvas_login": self.canvas_login,
                "canvas_password": self.canvas_password,
                "user": self.user.pk}

    def __unicode__(self):
        return self.canvas_login
